import React, { Component } from 'react';
import { Text, Image, View, Dimensions } from 'react-native';

import { createAppContainer } from 'react-navigation';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createStackNavigator } from 'react-navigation-stack';
import { createDrawerNavigator } from 'react-navigation-drawer';

import Login from '../Component/Login/Login';
import ForgotPassword from '../Component/Login/ForgotPassword';
import SplashScreen from '../Component/Login/SplashScreen';
import SignUp from '../Component/Login/SignUp';

import { Routes } from "./Routes";

import { YellowBox } from 'react-native';

YellowBox.ignoreWarnings([
    'Warning: componentWillMount is deprecated',
    'Warning: componentWillUpdate is deprecated',
    'Warning: componentWillReceiveProps is deprecated',
    'Module RCTImageLoader requires',
    'Warning: Async Storage has been extracted from react-native core'
]);

const { width, height } = Dimensions.get('window');


const BaseNavigatorContainerFromLogin = createAppContainer(
    createStackNavigator({
        [Routes.SplashScreen]: {
            screen: SplashScreen,
        },
        [Routes.Login]: {
            screen: Login
        },
        [Routes.ForgotPassword]: {
            screen: ForgotPassword
        },
        [Routes.SignUp]: {
            screen: SignUp
        },
        
    }, {
        headerMode: 'none'
    })
);


class BaseNavigator extends Component {

    constructor(props) {
        super(props);
        this.state = {
            BadgeCount: 0
        }
    }

    render() {

        return (
            <BaseNavigatorContainerFromLogin />
        )
    }
}

export { BaseNavigator };