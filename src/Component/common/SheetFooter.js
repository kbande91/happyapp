import React, { Component } from 'react';
import {
    Text,
    View,
    TouchableOpacity,
    Image
} from 'react-native';
import AppConstants from '../../Common/AppConstants';

const SheetFooter = ({  engine_no, chassis_no,  }) => {
    return (
        <View style={{ borderTopColor: '#501429', borderTopWidth: 1, backgroundColor: '#bebebe', justifyContent: 'center', alignItems: 'center' }}>
            < View style={{ padding: 10, marginBottom: 20, justifyContent: 'space-between', flexDirection: 'row', }}>
                    <Text style={styles.titleStyle}>
                        Engine #: {engine_no}
                    </Text>
                    <Text style={styles.titleStyle}>
                        Chassis #: {chassis_no}
                    </Text>
                </View>
        </View >
    )

}

const styles = {
    viewStyle: {
        padding: 10,
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
    },
    titleStyle: {
        color: '#3e3e3e',
        fontFamily: AppConstants.mediumFont,
        fontSize: AppConstants.titleSize,
        flex: 1,
        textAlign: 'center'
    }
}

export { SheetFooter };