import React, { Component } from 'react';
import {
    Text,
    View,
    TouchableOpacity,
    Image
} from 'react-native';
import AppConstants from '../../Common/AppConstants';

const Footer = ({ button_disable, userName, onPressLeft, nextBtnTitle, onPressRight, next_disabled = true, backVisible, onPressBack}) => {
    return (
        <View style={{ borderTopColor: '#501429', borderTopWidth: 1,  backgroundColor:'#bebebe',  justifyContent: 'center', alignItems: 'center' }}>
            <View style={[styles.viewStyle]}>
                {backVisible == true ?
                    <TouchableOpacity disabled={button_disable} style={{ borderRadius: 5, marginHorizontal: 20, paddingVertical: 7, flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: '#501429' , borderColor: '#501429', borderWidth: 0.5 }} onPress={onPressBack}>
                        <Text style={{ fontFamily: AppConstants.mediumFont, fontSize: AppConstants.titleSize, color: '#fff' }}>
                            Back
                        </Text>
                    </TouchableOpacity>
                    :
                    null
                }
                <TouchableOpacity disabled={next_disabled} style={{ borderRadius: 5, marginHorizontal: 20, paddingVertical: 7, flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: next_disabled == false ? '#501429' : '#ccc', borderColor: '#501429', borderWidth: 0.5 }} onPress={onPressLeft}>
                    <Text style={{ fontFamily: AppConstants.mediumFont, fontSize: AppConstants.titleSize, color: '#fff' }}>
                        {nextBtnTitle}
                    </Text>
                </TouchableOpacity>

                <TouchableOpacity disabled={button_disable} style={{ borderRadius: 5, marginHorizontal: 20, flex: 1, paddingVertical: 7, justifyContent: 'center', alignItems: 'center', backgroundColor: '#501429', borderColor: '#501429', borderWidth: 0.5 }} onPress={onPressRight}>
                    <Text style={{ fontFamily: AppConstants.mediumFont, fontSize: AppConstants.titleSize, color: '#fff' }}>
                        Cancel
                    </Text>
                </TouchableOpacity>
            </View>
            <View>
                <Text style={styles.titleStyle}>
                    Vehicle Examiner: {userName}
                </Text>
            </View>
        </View>
    )

}

const styles = {
    viewStyle: {
        padding: 10,
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
    },
    titleStyle: {
        color: '#3e3e3e',
        fontFamily: AppConstants.mediumFont,
        fontSize: AppConstants.titleSize
    }
}

export { Footer };