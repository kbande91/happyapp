import React, { Component } from 'react';
import { Modal, Text, TouchableOpacity, View, Alert, TextInput, Dimensions, FlatList } from 'react-native';
const { width, height } = Dimensions.get('window');
var FloatingLabel = require('react-native-floating-labels');
import { Dropdown } from 'react-native-material-dropdown';
import AppConstants from '../../Common/AppConstants';


const CustomDropDown = ({ actionOn, modalVisible, onPressClose, socialMediaValue = '', profileUrlValue = '', onAddPress, onChangeText, socialMediaData, selectedSocialMedia, onChangeUrlLabel, onChangeUrl }) => {
    return (
        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Modal
                animationType="slide"
                transparent={true}
                visible={modalVisible}
                onRequestClose={() => {
                    console.log('Modal has been closed.');
                }}>
                <View style={{ flex: 1, padding: 40, backgroundColor: '#e4e5e7', alignItems: 'center' }}>
                    <View style={{ marginTop: 20, alignItems: 'flex-end', width: width * 0.8, }}>
                        <TouchableOpacity style={{ height: 30 }} onPress={onPressClose}>
                            <Text style={{}}>Close</Text>
                        </TouchableOpacity>
                    </View>
                    {actionOn == 'SocailMedia' ?
                        < View style={{ marginTop: 20, }}>
                            <FloatingLabel
                                value={socialMediaValue}
                                labelStyle={styles.labelInput}
                                inputStyle={styles.input}
                                style={styles.formInput}>
                                Social Media
                            </FloatingLabel>
                            <Dropdown
                                labelHeight={10}
                                label=''
                                value={''}
                                textColor={AppConstants.blueColor}
                                containerStyle={{ height: 40, top: -55, marginBottom: -40, }}
                                data={socialMediaData}
                                inputContainerStyle={{ borderBottomColor: 'transparent', }}
                                onChangeText={(value, index, data) => selectedSocialMedia(value, index, data)}
                            />
                            <FloatingLabel
                                onChangeText={text => onChangeText(text)}
                                value={profileUrlValue}
                                labelStyle={styles.labelInput}
                                inputStyle={styles.input}
                                style={styles.formInput}>
                                Profile Url
                            </FloatingLabel>
                            <TouchableOpacity style={{ height: 30 }} onPress={onAddPress}>
                                <Text style={{ fontSize: 16, fontWeight: 'bold' }}>Add Item</Text>
                            </TouchableOpacity>
                        </View> :
                        < View style={{ marginTop: 20, }}>
                            <FloatingLabel
                                onChangeText={text => onChangeUrlLabel(text)}
                                value={socialMediaValue}
                                labelStyle={styles.labelInput}
                                inputStyle={styles.input}
                                style={styles.formInput}>
                                Url Label
                            </FloatingLabel>
                            <FloatingLabel
                                onChangeText={text => onChangeUrl(text)}
                                value={profileUrlValue}
                                labelStyle={styles.labelInput}
                                inputStyle={styles.input}
                                style={styles.formInput}>
                                Url
                            </FloatingLabel>
                            <TouchableOpacity style={{ height: 30 }} onPress={onAddPress}>
                                <Text style={{ fontSize: 16, fontWeight: 'bold' }}>Add Item</Text>
                            </TouchableOpacity>
                        </View>
                    }

                </View>
            </Modal>
        </View >
    );

}

const styles = {
    labelInput: {
        color: AppConstants.blueColor,
        fontSize: 14,
        backgroundColor: '#e4e5e7',
        marginTop: 3,
        marginLeft: 10,
        paddingHorizontal: 5,
        position: 'absolute'
    },
    formInput: {
        borderWidth: 1,
        borderColor: AppConstants.blueColor,
        marginVertical: 15,
        borderRadius: 5,
        height: 40,
    },
    input: {
        borderWidth: 0,
        fontSize: 16,
        color: AppConstants.blueColor,
        paddingLeft: 15,
        marginTop: 0,
        // backgroundColor: "blue"
    },
}



export { CustomDropDown };