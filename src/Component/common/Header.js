import React, { Component } from 'react';
import {
    Text,
    View,
    TouchableOpacity,
    Image
} from 'react-native';
import AppConstants from '../../Common/AppConstants';

const Header = ({ header_title, onPressLeft, onPressRight, only_title = false, titleVisible = true, backButton = false, homeButton = false, }) => {
    if (only_title === false) {
        return (
            <View style={[styles.viewStyle]}>
                <TouchableOpacity style={{ paddingLeft: 10, flex: 1, justifyContent: 'center', alignItems: 'flex-start' }} onPress={onPressLeft}>
                    <Image style={{ height: 25, width: 25, resizeMode: 'contain', tintColor: '#fff' }} source={backButton == false ? require('../../images/menu.png') : require('../../images/back.png')} />
                </TouchableOpacity>
                <View style={{ flex: 4, justifyContent: 'center', alignItems: 'center' }}>
                    {titleVisible === true ?
                        <Text style={styles.titleStyle}>
                            {header_title}
                        </Text>
                        :
                        null
                        // <Image style={{ height: 25, width: 74, resizeMode: 'contain' }} source={require('../../images/logo.png')} />
                    }
                </View>
                <TouchableOpacity style={{ paddingRight: 10, flex: 1, justifyContent: 'center', alignItems: 'flex-end' }} onPress={onPressRight}>
                    {homeButton == true ?
                        <Image style={{ height: 25, width: 25, resizeMode: 'contain' }} source={require('../../images/pencil_w.png')} />
                        : null
                    }
                </TouchableOpacity>
            </View>
        )
    }
    else {
        return (
            <View style={[styles.viewStyle]}>
                <TouchableOpacity style={{ paddingLeft: 10, flex: 1, justifyContent: 'center', alignItems: 'flex-start' }} >
                </TouchableOpacity>
                <View style={[styles.viewStyle, { justifyContent: 'center' }]}>
                    <Text style={styles.titleStyle}>
                        {header_title}
                    </Text>
                </View>
                <TouchableOpacity style={{ paddingRight: 10, flex: 1, justifyContent: 'center', alignItems: 'flex-end' }} onPress={onPressRight}>
                    {homeButton == true ?
                        <Image style={{ height: 25, width: 25, resizeMode: 'contain' }} source={require('../../images/home.png')} />
                        : null
                    }
                </TouchableOpacity>
            </View>
        )
    }

}

const styles = {
    viewStyle: {
        backgroundColor: '#1d428a',
        height: 44,
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',

    },
    titleStyle: {
        color: '#fff',
        fontFamily: AppConstants.mediumFont,
        fontSize: AppConstants.titleSize
    }
}

export { Header };