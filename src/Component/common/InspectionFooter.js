import React, { Component } from 'react';
import {
    Text,
    View,
    TouchableOpacity,
    Image
} from 'react-native';
import AppConstants from '../../Common/AppConstants';

const InspectionFooter = ({ buttonTitle, engine_no, chassis_no, onPressLeft, onPressRight, onPressBack, next_disabled = true, backVisible = false, infoVisible = true, cancel_disabled = false }) => {
    return (
        <View style={{ borderTopColor: '#501429', borderTopWidth: 1, backgroundColor: '#bebebe', justifyContent: 'center', alignItems: 'center' }}>
            <View style={[styles.viewStyle, { marginBottom: infoVisible == true ? 0 : 20, }]}>
                {backVisible == true ?
                    <TouchableOpacity style={{ borderRadius: 5, marginHorizontal: 20, paddingVertical: 7, flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: '#501429', borderColor: '#501429', borderWidth: 0.5 }} onPress={onPressBack}>
                        <Text style={{ fontFamily: AppConstants.mediumFont, fontSize: AppConstants.titleSize, color: '#fff' }}>
                            Back
                        </Text>
                    </TouchableOpacity>
                    :
                    null
                }
                <TouchableOpacity disabled={next_disabled} style={{ borderRadius: 5, marginHorizontal: 20, paddingVertical: 7, flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: next_disabled == false ? '#501429' : '#ccc', borderColor: '#501429', borderWidth: 0.5 }} onPress={onPressLeft}>
                    <Text style={{ fontFamily: AppConstants.mediumFont, fontSize: AppConstants.titleSize, color: '#fff' }}>
                        {buttonTitle}
                    </Text>
                </TouchableOpacity>

                <TouchableOpacity disabled={cancel_disabled} style={{ borderRadius: 5, marginHorizontal: 20, flex: 1, paddingVertical: 7, justifyContent: 'center', alignItems: 'center', backgroundColor: cancel_disabled == false ? '#501429' : '#ccc', borderColor: '#501429', borderWidth: 0.5 }} onPress={onPressRight}>
                    <Text style={{ fontFamily: AppConstants.mediumFont, fontSize: AppConstants.titleSize, color: '#fff' }}>
                        Cancel
                    </Text>
                </TouchableOpacity>
            </View>
            {infoVisible == true ?
                < View style={{ padding: 10, marginBottom: 20, justifyContent: 'space-between', flexDirection: 'row', }}>
                    <Text style={styles.titleStyle}>
                        Engine #: {engine_no}
                    </Text>
                    <Text style={styles.titleStyle}>
                        Chassis #: {chassis_no}
                    </Text>
                </View>
                : null
            }
        </View >
    )

}

const styles = {
    viewStyle: {
        padding: 10,
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
    },
    titleStyle: {
        color: '#3e3e3e',
        fontFamily: AppConstants.mediumFont,
        fontSize: AppConstants.titleSize,
        flex: 1,
        textAlign: 'center'
    }
}

export { InspectionFooter };