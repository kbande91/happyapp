import React from 'react';
import { Dimensions, View, Text, Image, ImageBackground, Platform } from 'react-native';
import { Routes } from '../../navigation/Routes';
import {
    getAsyncAccessToken,
    getAsyncRefreshToken
} from '../../Common/Helpers/SessionHelper';
var redirect = false;

class LTASplashScreen extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            width: Dimensions.get('window').width,
            height: Dimensions.get('window').height,
        }

        this.onLayout = this.onLayout.bind(this);

    }

    onLayout(e) {
        this.setState({
            width: Dimensions.get('window').width,
            height: Dimensions.get('window').height,
        });
    }

    async componentDidMount() {
        let AccessToken = await getAsyncAccessToken();
        setTimeout(() => {
            if (redirect == false) {
                redirect = true
                if (AccessToken == null) {
                    this.props.navigation.replace(Routes.Login);
                }
                else {
                    this.props.navigation.replace(Routes.Profile);
                }
            }
        }, 4000);
        redirect = false
    }

    render() {
        return (
            <ImageBackground onLayout={this.onLayout} source={ require('../../images/splash.png')} style={{ width: '100%', height: '100%', justifyContent: 'flex-end', alignItems: 'center' }}>
                
            </ImageBackground>
        )
    }
};

export default LTASplashScreen;
