import React, { Fragment } from 'react';
import {
    View,
    Text,
    Image,
    TextInput,
    TouchableOpacity,
    YellowBox,
    ActivityIndicator,
    Dimensions,
    ScrollView,
    ImageBackground,
    Platform,
} from 'react-native';
import styles from './style';
import { fetchWrapper, ApiError } from '../../Common/Helpers/fetchAPIWrapper';
import { Dropdown } from 'react-native-material-dropdown';
import { Routes } from '../../navigation/Routes';
import { NavigationActions } from 'react-navigation';
import AppConstants from '../../Common/AppConstants';
var FloatingLabel = require('react-native-floating-labels');

class SignUp extends React.Component {

    constructor(props) {
        super(props);

        YellowBox.ignoreWarnings([
            'Warning: componentWillMount is deprecated',
            'Warning: componentWillReceiveProps is deprecated',
        ]);

        this.state = {
            
        }

        this.onLayout = this.onLayout.bind(this);
    }

    componentDidMount() {

    }

    componentWillUnmount() {
    }

    onLayout(e) {
        this.setState({
            width: Dimensions.get('window').width,
            height: Dimensions.get('window').height,
        });
    }

    onSubmitPress() {
       
    }

    onSignPress() {
        this.props.navigation.goBack(null);
    }

    render() {
        return (
            <View onLayout={this.onLayout} style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                <View style={{ justifyContent: 'center', borderRadius: 5, backgroundColor: '#e4e5e7', padding: 20, width: this.state.width * 0.9 }}>
                    <ScrollView >
                        
                        
                    </ScrollView>
                </View>
               
            </View>
        )
    }
};

export default SignUp;
