import React, { Fragment } from 'react';
import {
  View,
  Text,
  Image,
  TextInput,
  TouchableOpacity,
  YellowBox,
  ActivityIndicator,
  Dimensions,
  ScrollView,
  ImageBackground,
  Platform,
} from 'react-native';
import styles from './style';
import { fetchWrapper, ApiError } from '../../Common/Helpers/fetchAPIWrapper';

import { Routes } from '../../navigation/Routes';
import { NavigationActions } from 'react-navigation';

import AppConstants from '../../Common/AppConstants';
import { CheckBox } from 'react-native-elements';

var FloatingLabel = require('react-native-floating-labels');


class ForgotPassword extends React.Component {

  constructor(props) {
    super(props);

    YellowBox.ignoreWarnings([
      'Warning: componentWillMount is deprecated',
      'Warning: componentWillReceiveProps is deprecated',
    ]);

    this.state = {
      UserName: "",        // 909310
      Email: "",
      RememberChk: false,
      width: Dimensions.get('window').width,
      height: Dimensions.get('window').height,
      spinner: false
    }

    this.onLayout = this.onLayout.bind(this);
  }

  componentDidMount() {

  }

  componentWillUnmount() {
  }

  onLayout(e) {
    this.setState({
      width: Dimensions.get('window').width,
      height: Dimensions.get('window').height,
    });
  }

  onSubmitPress() {

  }

  onSignPress() {
    this.props.navigation.goBack(null);
  }

  render() {
    return (
      <View onLayout={this.onLayout} style={{ flex: 1, alignItems: 'center', }}>
        <ScrollView >
          <View style={{ marginTop: this.state.height * 0.3, justifyContent: 'center', borderRadius: 5, backgroundColor: '#e4e5e7', padding: 20, width: this.state.width * 0.9 }}>
            <View style={{ alignItems: 'center', marginBottom: 20 }}>
              <Text style={{ fontSize: 24, color: AppConstants.blueColor, marginBottom: 10 }}>
                Officiating Opportunities
              </Text>
              <Text style={{ fontSize: 22, color: AppConstants.redColor, }}>
                Forgot Password
              </Text>
            </View>
            <FloatingLabel
              onChangeText={(value) => this.setState({UserName: value})}
              value={this.state.UserName}
              labelStyle={styles.labelInput}
              inputStyle={styles.input}
              style={styles.formInput}>
              Username
              </FloatingLabel>
            <View style={{ alignItems: 'center',  }}>
              <Text style={{ fontSize: 22, color: AppConstants.redColor, }}>
                OR
              </Text>
            </View>
            <FloatingLabel
              onChangeText={(value) => this.setState({Email: value})}
              value={this.state.Email}
              labelStyle={styles.labelInput}
              inputStyle={styles.input}
              style={styles.formInput}>
              Email
            </FloatingLabel>


            <TouchableOpacity onPress={() => { this.onSubmitPress() }} style={{ alignItems: 'center', marginTop: 10, paddingHorizontal: 10, paddingVertical: 10, borderRadius: 5, backgroundColor: AppConstants.redColor }}>
              <Text style={{ color: '#fff', fontSize: 14, fontWeight: 'bold' }}>Submit</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => { this.onSignPress() }} style={{ alignItems: 'center', marginTop: 10, paddingHorizontal: 10, paddingVertical: 5, borderRadius: 5, }}>
              <Text style={{ color: AppConstants.blueColor, fontSize: 14 }}>Sign In</Text>
            </TouchableOpacity>

          </View>
        </ScrollView>
      </View>
    )
  }
};

export default ForgotPassword;
