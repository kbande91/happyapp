import React, { Fragment } from 'react';
import {
  View,
  Text,
  Image,
  TextInput,
  TouchableOpacity,
  YellowBox,
  ActivityIndicator,
  Dimensions,
  ScrollView,
  ImageBackground,
  Platform,
} from 'react-native';
import styles from './style';
import { fetchWrapper, ApiError } from '../../Common/Helpers/fetchAPIWrapper';
import {
  setAsyncAccessToken,
  setAsyncSaveUsername,
  setAsyncSavePassword,
  getAsyncUsername,
  getAsyncPassword,
  setAsyncUserRole,
  setMasterData
} from '../../Common/Helpers/SessionHelper';
import { Routes } from '../../navigation/Routes';
import AppConstants from '../../Common/AppConstants';
import { CheckBox } from 'react-native-elements';
var FloatingLabel = require('react-native-floating-labels');


class Login extends React.Component {

  constructor(props) {
    super(props);

    YellowBox.ignoreWarnings([
      'Warning: componentWillMount is deprecated',
      'Warning: componentWillReceiveProps is deprecated',
    ]);

    this.state = {
      UserName: "",        // 909310
      Password: "",
      RememberChk: false,
      width: Dimensions.get('window').width,
      height: Dimensions.get('window').height,
    }

    this.onLayout = this.onLayout.bind(this);
  }

  componentDidMount() {

  }


  componentWillUnmount() {
  }

  onLayout(e) {
    this.setState({
      width: Dimensions.get('window').width,
      height: Dimensions.get('window').height,
    });
  }


  LoginUser = () => {
    // this.props.navigation.replace(Routes.Profile);
    if (this.state.UserName === '') {
      alert('Please enter User Name');
      return false;
    }
    else if (this.state.Password === '') {
      alert('Please enter Password');
      return false;
    }
    else {
     
    }


  }

  SignUpPress() {
    this.props.navigation.navigate(Routes.SignUp);
  }

  render() {
    return (
      <View onLayout={this.onLayout} style={{ flex: 1, alignItems: 'center', }}>
        <ScrollView >
          <View style={{ marginTop: this.state.height * 0.3, justifyContent: 'center', borderRadius: 5, backgroundColor: '#e4e5e7', padding: 20, width: this.state.width * 0.9 }}>
           
            <FloatingLabel
              onChangeText={(value) => this.setState({ UserName: value })}
              value={this.state.UserName}
              labelStyle={styles.labelInput}
              inputStyle={styles.input}
              style={styles.formInput}>
              Username
              </FloatingLabel>
            <FloatingLabel
              onChangeText={(value) => this.setState({ Password: value })}
              secureTextEntry
              value={this.state.Password}
              labelStyle={styles.labelInput}
              inputStyle={styles.input}
              style={styles.formInput}>
              Password
            </FloatingLabel>
           
            <TouchableOpacity onPress={() => { this.LoginUser() }} style={{ alignItems: 'center', marginTop: 10, paddingHorizontal: 10, paddingVertical: 10, borderRadius: 5, backgroundColor: AppConstants.redColor }}>
              <Text style={{ color: '#fff', fontSize: 14, fontWeight: 'bold' }}>Login</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => { this.onPressForgotPassword() }} style={{ alignItems: 'center', marginTop: 10, paddingHorizontal: 10, paddingVertical: 5, borderRadius: 5, }}>
              <Text style={{ color: AppConstants.blueColor, fontSize: 14 }}>Forgot Password</Text>
            </TouchableOpacity>
            <View style={{ alignItems: 'center', }}>
              <TouchableOpacity onPress={() => { this.SignUpPress() }} style={{ alignItems: 'center', marginTop: 10, paddingHorizontal: 10, paddingVertical: 10, borderRadius: 5, backgroundColor: AppConstants.blueColor }}>
                <Text style={{ color: '#fff', fontSize: 14, fontWeight: 'bold' }}>Sign Up</Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
        
        
      </View>
    )
  }
};

export default Login;
