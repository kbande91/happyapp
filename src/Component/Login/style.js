import {
  StyleSheet,
  Platform,
  Dimensions,
} from 'react-native';
import AppConstants from '../../Common/AppConstants';

export default StyleSheet.create({
  labelInput: {
    color: AppConstants.blueColor,
    fontSize: 14,
    backgroundColor: '#e4e5e7',
    marginTop: 3,
    marginLeft: 10,
    paddingHorizontal: 5,
    position: 'absolute'
  },
  formInput: {
    borderWidth: 1,
    borderColor: AppConstants.blueColor,
    marginVertical: 15,
    borderRadius: 5,
    height: 40,
  },
  input: {
    borderWidth: 0,
    fontSize: 16,
    color: AppConstants.blueColor,
    paddingLeft: 15,
    marginTop: 0,
    // backgroundColor: "blue"
  },
  pickerStyle: {
    height: 150,
    width: "80%",
    color: '#344953',
    justifyContent: 'center',
  }

});
