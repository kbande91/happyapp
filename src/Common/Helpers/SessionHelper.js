import AsyncStorage from '@react-native-community/async-storage';

export const setAsyncAccessToken = async (value) =>{
  await AsyncStorage.setItem('AccessToken', value);
}

export const setAsyncUserRole = async (value) =>{
  await AsyncStorage.setItem('UserRole', value);
}

export const setAsyncSaveUsername = async (value) =>{
  await AsyncStorage.setItem('Username', value);
}

export const setAsyncSavePassword = async (value) =>{
  await AsyncStorage.setItem('Password', value);
}

export const getAsyncAccessToken = async() =>{
  return await AsyncStorage.getItem('AccessToken');
}

export const getAsyncUserRole = async() =>{
  return await AsyncStorage.getItem('UserRole');
}

export const getAsyncUsername = async() =>{
  return await AsyncStorage.getItem('Username');
}

export const getAsyncPassword = async() =>{
  return await AsyncStorage.getItem('Password');
}

//

export const setMasterData = async (value) =>{
  await AsyncStorage.setItem('MasterData', value);
}

export const getMasterData = async() =>{
  return await AsyncStorage.getItem('MasterData');
}
