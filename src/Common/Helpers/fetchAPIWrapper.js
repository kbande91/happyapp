import AppConstants from '../AppConstants';
import {
  
  getAsyncAccessToken, setAsyncAccessToken,
} from '../Helpers/SessionHelper';
import { Routes } from '../../navigation/Routes';
import { StackActions, NavigationActions } from 'react-navigation';

//const API_URL = AppConstants.API_URL;

// Custom API error to throw
export function ApiError(message, data, status) {
  let response = null;
  let isObject = false;

  // We are trying to parse response
  try {
    response = JSON.parse(data);
    isObject = true;
  } catch (e) {
    response = data;
  }

  this.response = response;
  this.message = message;
  this.status = status;
  this.toString = function () {
    return `${this.message}\nResponse:\n${isObject ? JSON.stringify(this.response, null, 2) : this.response}`;
  };
}

// API wrapper function
export const fetchWrapper = async (path, userOptions = {}, navigation) => {
  // Define default options
  const defaultOptions = {

  };
  let AccessToken = await getAsyncAccessToken();
  // Define default headers
  const defaultHeaders = {
    "Authorization": 'Bearer ' + AccessToken,
  };


  const options = {
    // Merge options
    ...defaultOptions,
    ...userOptions,
    // Merge headers
    headers: {
      ...defaultHeaders,
      ...userOptions.headers,
    },
  };

  // Build Url
  const url = path;                    // `${ AppConstants.API_URL }/${ path }`;

  // Detect is we are uploading a file
  const isFile = options.body instanceof File;

  // Stringify JSON data
  // If body is not a file

  if (options.body && typeof options.body === 'object' && !isFile) {
    options.body = JSON.stringify(options.body);
  }

  // Variable which will be used for storing response
  let response = null;
  return await fetch(url, options)
    .then(responseObject => {
      // Saving response for later use in lower scopes
      response = responseObject;

      if (response.status === 401) {
        alert('Session Expired. Please Login Again.')
        setAsyncAccessToken('');
        
        let resetAction = StackActions.reset({
          index: 0,
          actions: [
            NavigationActions.navigate({ routeName: Routes.Login })
          ],
        });
        navigation.dispatch(resetAction);
      }
      if(response.status == 500){
        throw new Error(response.status);
      }
      else{
        return response.json();
      }
      // if (response.status < 200 || response.status >= 300) {
      //   return response.text();  // Get response as text
      // }
        // Get response as json
    })
    
    .then(parsedResponse => {
      // Check for HTTP error codes
      // if (response.status < 200 || response.status >= 300) {
      //   throw parsedResponse;
      // }
      return parsedResponse;
    })
    .catch(error => {
      return error
      // if(response != null){
      //   if (response.status == 500){
      //     alert('Server error: 500')
      //   }
      // }
      // else if (error.message == 'Network request failed') {
      //   alert('Please check internet connection')
      // }
      // else if (error) {   // If response exists it means HTTP error occured
      //   throw new ApiError(`Request failed with status ${response.status}.`, error, response.status);
      // } else {
      //   throw new ApiError(error.toString(), null, 'REQUEST_FAILED');
      // }
    });
};

export default fetchWrapper;