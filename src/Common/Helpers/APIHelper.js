

//const url = 'https://randomuser.me/api/?results=05';
//method: 'POST',
// headers: {
//   Accept: 'application/json',
//   'Content-Type': 'application/json',
// },
// body: JSON.stringify({
//   firstParam: 'yourValue',
//   secondParam: 'yourOtherValue',
// }),


// using request object
export const fetchAsyncWithFullRequestObj = (url,requestType,headers,body) =>
{
    let response =  fetch(url, { method:requestType, headers:headers, body:body })
    .then(function (response) {
        if (!response.ok) {
            throw Error(response.statusText);
        }
        return response.json()
    })
    .then((responseData) => {           
        return responseData;            
    })
    .catch((error) => {          
        return error;
    })
}

// using request object
export const fetchAsyncWithRequestObj = (request) =>
{
    let response =  fetch(request)
    .then(function (response) {
        if (!response.ok) {
            throw Error(response.statusText);
        }
        return response.json()
    })
    .then((responseData) => {           
        return responseData;            
    })
    .catch((error) => {          
        return error;
    })
}

// using fetchData object
export const fetchAsyncWithDataRequestObj = (url,fetchData) =>
{
    let response = fetch(url,fetchData)
    .then((response) => {
        if (!response.ok) {
            throw Error(response.statusText);
        }
        return response.json().then(data => ({
            data: data,
            status: response.status
        }))
    })
    .then((responseData) => {           
        alert(responseData.data.Accesstoken);  
        return responseData; 
    })
    .catch((error) => {          
         alert(JSON.stringify(error));
    })
    .done()  
}

