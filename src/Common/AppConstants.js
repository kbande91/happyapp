const AppConstants = {
    
    // API URLs
    // API_URL: 'https://vri.lta.com.fj/', //Prod
    // API_URL: 'https://vrenf.lta.com.fj/', //UAT ENF     
    // API_URL: 'https://testvri.lta.com.fj/', //UAT VRI
    API_URL: 'https://nbaofficialsapi-dev.azurewebsites.net/api/', //QA_VRI

    API_MobileUserLogin: 'User/ValidateUser',

    //Font_Family


    //Font_Size
    infolabelSize: 10,
    labelSize: 12,
    infoSize: 14,
    inputSize: 16,
    titleSize: 18,
    buttonSize: 20,
    headerSize: 22,

    blueColor: '#054b82',
    redColor: '#c8102e',
    darkBlue: '#17408b',

    userInfo: {}
}

export default AppConstants;